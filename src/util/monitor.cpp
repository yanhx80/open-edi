/* @file  monitor.cpp
 * @date  <date>
 * @brief <Descriptions>
 *
 * Copyright (C) 2020 NIIC EDA
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD license.  See the LICENSE file for details.
 */

#include "util/monitor.h"

namespace open_edi {
namespace util {

/**
 * @brief Global variables for non-member function.
 *        In the system there should be only one MonitorManager.
 */
MonitorManager kMonitorManager;

/**
 * @brief Global variables for peak memory timer.
 *        It is working for a linux time thread.
 */
MonitorInformation kPeakMemoryTimer;

/**
 * @brief Construct a new Monitor:: Monitor object.
 *        get start time and set monitor state.
 */
Monitor::Monitor() {
    if ( gettimeofday(&start_time_, NULL) < 0 ) {
        message->issueMsg(kError, "Monitor: gettimeofday() return code %d\n",
                            errno);
    }
    if ( 0 != getrusage(RUSAGE_SELF, &start_usage_) ) {
        message->issueMsg(kError, "Monitor: getrusage() return code %d\n",
                            errno);
    }

    state_ = kMonitorRunning;
}

/**
 * @brief Construct a new Monitor Information:: Monitor Information object.
 *        init member variables.
 */
MonitorInformation::MonitorInformation() {
    initPeakMemory();
    pause_elapsed_t_ = 0;
    pause_user_cpu_t_ = 0;
    pause_sys_cpu_t_ = 0;
}

/**
 * @brief calculate cpu time, including user time and system time.
 * 
 * @param start_usage 
 */
void MonitorInformation::setCpuTime(struct rusage start_usage) {
    struct rusage current_usage;
    if ( 0 != getrusage(RUSAGE_SELF, &current_usage) ) {
        message->issueMsg(kError, "setCpuTime: getrusage()"
                                    " return code %d\n", errno);
    }

    user_cpu_time_ = current_usage.ru_utime.tv_sec
            + static_cast<double>(current_usage.ru_utime.tv_usec) / 1000000.0
            - start_usage.ru_utime.tv_sec
            - static_cast<double>(start_usage.ru_utime.tv_usec) / 1000000.0
            - pause_user_cpu_t_;
    sys_cpu_time_ = current_usage.ru_stime.tv_sec
            + static_cast<double>(current_usage.ru_stime.tv_usec) / 1000000.0
            - start_usage.ru_stime.tv_sec
            - static_cast<double>(start_usage.ru_stime.tv_usec) / 1000000.0
            - pause_sys_cpu_t_;
    cpu_time_ = user_cpu_time_ + sys_cpu_time_;
}

/**
 * @brief set peak memory = 0.
 */
void MonitorInformation::initPeakMemory() {
    peak_physical_mem_ = 0;
    peak_virtual_mem_ = 0;
}

/**
 * @brief get virtural memory and physical memory from /proc/<pid>/status.
 *        get peak memory from them.
 */
void MonitorInformation::setMemory() {
    pid_t pid = getpid();
    char file_name[64] = {0};

    char line_buff[256] = {0};
    char name[32] = {0};
    uint64_t vmsize = 0;
    uint64_t vmhwm = 0;
    uint64_t vmrss = 0;
    char file_unit[32] = {0};

    FILE *fp = NULL;
    snprintf(file_name, sizeof(file_name), "/proc/%d/status", pid);
    fp = fopen(file_name, "r");
    if (!fp) {
        message->issueMsg(kError, "setMemory: open file error"
                                    " return code %d\n", errno);
        exit(1);
    }
    while (NULL != fgets(line_buff, sizeof(line_buff), fp)) {
        sscanf(line_buff, "%s", name);
        if (0 == strncmp(name, "VmSize", 6)) {
            sscanf(line_buff, "%s %d %s", name, &vmsize, file_unit);
            virtual_mem_ = vmsize;
            continue;
        } else if (0 == strncmp(name, "VmHWM", 5)) {
            sscanf(line_buff, "%s %d %s", name, &vmhwm, file_unit);
            vmHWM_ = vmhwm;
            continue;
        } else if (0 == strncmp(name, "VmRSS", 5)) {
            sscanf(line_buff, "%s %d %s", name, &vmrss, file_unit);
            physical_mem_ = vmrss;
            break;
        }
    }
    fclose(fp);

    peak_virtual_mem_ =
        peak_virtual_mem_ >= virtual_mem_
            ? peak_virtual_mem_ : virtual_mem_;
    peak_physical_mem_ =
        peak_physical_mem_ >= physical_mem_
            ? peak_physical_mem_ : physical_mem_;
}

/**
 * @brief converse memory unit, return memory string.
 * 
 * @param unit 
 * @param mem 
 * @return std::string 
 */
std::string MonitorInformation::memoryUnitConversion(char unit[],
                                              uint64_t mem) {
    double mem_size = static_cast<double>(mem);
    std::string mem_str = std::to_string(mem);
    size_t length = mem_str.length();

    if (length > 3 && length <= 6) {
        mem_size /= 1024.0;
        strncpy(unit, "MiB", 3);
    } else if (length > 6 && length <= 9) {
        mem_size /= (1024.0 * 1024.0);
        strncpy(unit, "GiB", 3);
    } else if (length > 9 && length <= 12) {
        mem_size /= (1024.0 * 1024.0 * 1024.0);
        strncpy(unit, "TiB", 3);
    }

    std::stringstream ss;
    ss << std::fixed << std::setprecision(2) << mem_size;
    ss >> mem_str;
    ss.clear();
    if (5 == mem_str.length())
        mem_str.insert(0, " ");
    else if (4 == mem_str.length())
        mem_str.insert(0, "  ");

    return mem_str;
}

/**
 * @brief return VmHWM and set its unit.
 * 
 * @param unit 
 * @return std::string 
 */
std::string MonitorInformation::getVmHWM(char unit[]) {
    return memoryUnitConversion(unit, vmHWM_);
}

/**
 * @brief return virtural memory and set its unit.
 * 
 * @param unit 
 * @return std::string 
 */
std::string MonitorInformation::getVirtMemory(char unit[]) {
    return memoryUnitConversion(unit, virtual_mem_);
}

/**
 * @brief return physical memory and set its unit.
 * 
 * @param unit 
 * @return std::string 
 */
std::string MonitorInformation::getPhysMemory(char unit[]) {
    return memoryUnitConversion(unit, physical_mem_);
}

/**
 * @brief return peak virtural memory and set its unit.
 * 
 * @param unit 
 * @return std::string 
 */
std::string MonitorInformation::getPeakVirtMemory(char unit[]) {
    return memoryUnitConversion(unit, peak_virtual_mem_);
}

/**
 * @brief return peak physical memory and set its unit.
 * 
 * @param unit 
 * @return std::string 
 */
std::string MonitorInformation::getPeakPhysMemory(char unit[]) {
    return memoryUnitConversion(unit, peak_physical_mem_);
}

/**
 * @brief record current information, including elapsed time, cpu time and memory size.
 */
void Monitor::recordCurrentInfo() {
    current_info_.recordElapsedTime(start_time_);
    current_info_.setCpuTime(start_usage_);
    current_info_.setMemory();
}

/**
 * @brief set and get paused time.
 */
void Monitor::resumeCurrentInfo() {
    current_info_.recordPauseTime(start_time_, start_usage_);
}

/**
 * @brief calculate elapsed time,
 *        elapsed_time = current_time - start_time - p_elapsed_time,
 *        p_elapsed_time is the time spend on pause.
 * 
 * @param start_time 
 */
void MonitorInformation::recordElapsedTime(struct timeval start_time) {
    struct timeval current_time;
    if (gettimeofday(&current_time, NULL) < 0) {
        message->issueMsg(kError, "recordElapsedTime: gettimeofday()"
                                    " return code %d\n", errno);
    }
    elapsed_time_ = (current_time.tv_sec - start_time.tv_sec)
        + static_cast<double>(current_time.tv_usec - start_time.tv_usec)
        / 1000000.0 - pause_elapsed_t_;
}

/**
 * @brief get current information, including time and memory.
 * 
 * @return MonitorInformation& 
 */
MonitorInformation & Monitor::getCurrentInfo() {
    if (state_ == kMonitorRunning) {
        recordCurrentInfo();
    }
    return current_info_;
}

/**
 * @brief set time spend on pause.
 */
void MonitorInformation::recordPauseTime(struct timeval start_time,
                                    struct rusage start_usage) {
    struct timeval current_time;
    if ( gettimeofday(&current_time, NULL) < 0 ) {
        message->issueMsg(kError, "recordPauseTime: gettimeofday()"
                                    " return code %d\n", errno);
    }
    pause_elapsed_t_ = (current_time.tv_sec - start_time.tv_sec)
                    + static_cast<double>(current_time.tv_usec
                    - start_time.tv_usec) / 1000000.0
                    - elapsed_time_;
    struct rusage current_usage;
    if ( 0 != getrusage(RUSAGE_SELF, &current_usage) ) {
        message->issueMsg(kError, "recordPauseTime: getrusage()"
                                    " return code %d\n", errno);
    }
    pause_user_cpu_t_ = current_usage.ru_utime.tv_sec
            + static_cast<double>(current_usage.ru_utime.tv_usec) / 1000000.0
            - start_usage.ru_utime.tv_sec
            - static_cast<double>(start_usage.ru_utime.tv_usec) / 1000000.0
            - user_cpu_time_;
    pause_sys_cpu_t_ = current_usage.ru_stime.tv_sec
            + static_cast<double>(current_usage.ru_stime.tv_usec) / 1000000.0
            - start_usage.ru_stime.tv_sec
            - static_cast<double>(start_usage.ru_stime.tv_usec) / 1000000.0
            - sys_cpu_time_;
}

/**
 * @brief pause monitor.
 */
void Monitor::pause() {
    state_ = kMonitorPaused;
    recordCurrentInfo();
}

/**
 * @brief resume monitor.
 */
void Monitor::resume() {
    if (kMonitorPaused == state_)
        resumeCurrentInfo();
    state_ = kMonitorRunning;
}

/**
 * @brief reset monitor.
 */
void Monitor::reset() {
    if ( gettimeofday(&start_time_, NULL) < 0 ) {
        message->issueMsg(kError, "reset: gettimeofday() return code %d\n",
                            errno);
    }
    if ( 0 != getrusage(RUSAGE_SELF, &start_usage_) )
        message->issueMsg(kError, "reset: getrusage() return code %d\n",
                            errno);

    kPeakMemoryTimer.initPeakMemory();

    state_ = kMonitorRunning;
}

/**
 * @brief Construct a new Monitor Manager:: Monitor Manager object.
 */
MonitorManager::MonitorManager() {
    id_ = kInvalidMonitorId + 1;
    unused_num_id_ = kMaxNumMonitorId;

    strncpy(unit_virt_, "KiB", 3);
    strncpy(unit_phys_, "KiB", 3);
    strncpy(unit_vmHWM_, "KiB", 3);

    //  timer pthread
    periodTimerGetPeak();
}

/**
 * @brief Destroy the Monitor Manager:: Monitor Manager object.
 */
MonitorManager::~MonitorManager() {
    std::unordered_map<MonitorId, Monitor*>::iterator it = monitor_map_.begin();
    while (it !=  monitor_map_.end()) {
        delete it->second;
        ++it;
    }
    initTimer();
}

/**
 * @brief thread for getting peak memory.
 * 
 * @param signo 
 */
void MonitorManager::calculateMemory(int signo) {
    kPeakMemoryTimer.setMemory();
}

/**
 * @brief init linux timer.
 */
void MonitorManager::initTimer() {
    struct itimerval value;
    value.it_value.tv_sec = 0;
    value.it_value.tv_usec = 0;
    value.it_interval = value.it_value;

    if (0 != setitimer(ITIMER_REAL, &value, NULL)) {
        message->issueMsg(kError, "initTimer: setitimer()"
                                    " return code %d\n", errno);
        exit(1);
    }
}

/**
 * @brief start linux timer.
 */
void MonitorManager::periodTimerGetPeak() {
    if (SIG_ERR == signal(SIGALRM, calculateMemory)) {
        message->issueMsg(kError, "periodTimerGetPeak: signal()"
                                    " return code %d\n", errno);
        exit(1);
    }

    struct itimerval value;
    value.it_value.tv_sec = 3;
    value.it_value.tv_usec = 0;
    value.it_interval.tv_sec = 3;
    value.it_interval.tv_usec = 0;

    if (0 != setitimer(ITIMER_REAL, &value, NULL)) {
        message->issueMsg(kError, "periodTimerGetPeak: setitimer()"
                                    " return code %d\n", errno);
        exit(1);
    }
}

/**
 * @brief create new monitor.
 * 
 * @return MonitorId 
 */
MonitorId MonitorManager::createMonitor() {
    if (0 == unused_num_id_) {
        message->issueMsg(kError, "All monitor ids are used.\n");
        return kInvalidMonitorId;
    }
    MonitorId temp_id = id_;
    while (monitor_map_.find(temp_id) != monitor_map_.end()) {
        ++temp_id;
        if (temp_id == id_) {
            message->issueMsg(kError, "All monitor ids are used.\n");
            return kInvalidMonitorId;
        }
    }
    Monitor* monitor = new Monitor;
    monitor_map_[id_] = monitor;
    unused_num_id_--;

    return id_++;
}

/**
 * @brief pause monitor.
 * 
 * @param monitor_id 
 * @return true 
 * @return false 
 */
bool MonitorManager::pauseMonitor(MonitorId monitor_id) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    auto it = monitor_map_.find(monitor_id);
    if (it != monitor_map_.end()) {
        Monitor *monitor = it->second;
        monitor->pause();
        monitor->recordCurrentInfo();

        return true;
    }

    return false;
}

/**
 * @brief format time, 0:00:00.
 * 
 * @param t 
 * @return std::string 
 */
std::string MonitorManager::formatTime(double t) {
    if (t < 1.0) {
        std::string time_str;
        std::stringstream ss;
        ss << std::fixed
            << std::setprecision(3) << (t * 1000.0);
        ss >> time_str;
        ss.clear();
        return (time_str + "ms");
    }
    else { 
        int time = static_cast<int>(t);
        int hour, min, sec;
        hour = time / 3600;
        min = (time - 3600 * hour) / 60;
        sec = time - 3600 * hour - 60 * min;

        return (std::to_string(hour) + ":"
            + ((std::to_string(min).size()==1) ? "0" : "")
            + std::to_string(min) + ":"
            + ((std::to_string(sec).size()==1) ? "0" : "")
            + std::to_string(sec) + "  ");
    }
}

/**
 * @brief check resource types and then append to type str.
 * 
 * @param str 
 * @param resource_types 
 */
void MonitorManager::checkResourceTypes(std::string &type_str,
                                ResourceTypes resource_types,
                                MonitorInformation current_info) {
    if (resource_types & kElapsedTime) {
        type_str += " ElapTime:"
            + formatTime(current_info.getElapsedTime());
    }
    if (resource_types & kCpuTime) {
        type_str += " CpuTime:"
            + formatTime(current_info.getCpuTime());
    }
    if (resource_types & kUserCpuTime) {
        type_str += " UserCpuTime:"
            + formatTime(current_info.getUserCpuTime());
    }
    if (resource_types & kSysCpuTime) {
        type_str += " SysCpuTime:"
            + formatTime(current_info.getSysCpuTime());
    }
    if (resource_types & kPhysicalMem) {
        type_str += " PhysMem:"
            + current_info.getPhysMemory(unit_phys_)
            + unit_phys_;
    }
    if (resource_types & kPhysicalPeak) {
        type_str += " PhysPeak:"
            + kPeakMemoryTimer.getPeakPhysMemory(unit_phys_)
            + unit_phys_;
    }
    if (resource_types & kVirtualMem) {
        type_str += " VirtMem:"
            + current_info.getVirtMemory(unit_virt_)
            + unit_virt_;
    }
    if (resource_types & kVirtualPeak) {
        type_str += " VirtPeak:"
            + kPeakMemoryTimer.getPeakVirtMemory(unit_virt_)
            + unit_virt_;
    }
    if (resource_types & kVmHWM) {
        type_str += " VmHWM:"
            + current_info.getVmHWM(unit_vmHWM_)
            + unit_vmHWM_;
    }
}

/**
 * @brief output information in one line.
 * 
 * @param monitor_id 
 * @param resource_types 
 * @param description 
 * @return true 
 * @return false 
 */
bool MonitorManager::printProcessBar(MonitorId monitor_id,
                                   ResourceTypes resource_types,
                                   const char* description) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }

    auto it = monitor_map_.find(monitor_id);
    if (it != monitor_map_.end()) {
        Monitor *monitor = it->second;
        MonitorInformation current_info = monitor->getCurrentInfo();

        std::string type_str = description;
        checkResourceTypes(type_str, resource_types, current_info);

        message->info("%s%s", type_str.c_str(), "\r");

        fflush(stdout);
        return true;
    }
    message->issueMsg(kError, "printProcessBar: cannot find monitor by id %d\n",
                                               monitor_id);
    return false;
}

/**
 * @brief output information per line.
 * 
 * @param monitor_id 
 * @param resource_types 
 * @param description 
 * @return true 
 * @return false 
 */
bool MonitorManager::printMonitor(MonitorId monitor_id,
                                   ResourceTypes resource_types,
                                   const char* description) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }

    auto it = monitor_map_.find(monitor_id);
    if (it != monitor_map_.end()) {
        Monitor *monitor = it->second;
        MonitorInformation current_info = monitor->getCurrentInfo();

        std::string type_str = description;
        checkResourceTypes(type_str, resource_types, current_info);

        message->info("%s%s", type_str.c_str(), "\n");

        fflush(stdout);
        return true;
    }
    message->issueMsg(kError, "printMonitor: cannot find monitor by id %d\n",
                                               monitor_id);
    return false;
}

/**
 * @brief write output information to file openned by FILE *.
 * 
 * @param monitor_id 
 * @param resource_types 
 * @param fp 
 * @param description 
 * @return true 
 * @return false 
 */
bool MonitorManager::printMonitor(MonitorId monitor_id,
                                   ResourceTypes resource_types,
                                   FILE *fp,
                                   const char* description) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }

    auto it = monitor_map_.find(monitor_id);
    if (it != monitor_map_.end()) {
        Monitor *monitor = it->second;
        MonitorInformation current_info = monitor->getCurrentInfo();

        std::string type_str = description;
        checkResourceTypes(type_str, resource_types, current_info);

        fprintf(fp, "%s\n", type_str.c_str());

        return true;
    }
    message->issueMsg(kError, "printMonitor<FILE *>: cannot find"
                                " monitor by id %d\n", monitor_id);
    return false;
}

/**
 * @brief write output information to file openned by file stream.
 * 
 * @param monitor_id 
 * @param resource_types 
 * @param fp 
 * @param description 
 * @return true 
 * @return false 
 */
bool MonitorManager::printMonitor(MonitorId monitor_id,
                                   ResourceTypes resource_types,
                                   std::ofstream &fp,
                                   const char* description) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }

    auto it = monitor_map_.find(monitor_id);
    if (it != monitor_map_.end()) {
        Monitor *monitor = it->second;
        MonitorInformation current_info = monitor->getCurrentInfo();

        std::string type_str = description;
        checkResourceTypes(type_str, resource_types, current_info);

        fp << type_str.c_str() << '\n';

        return true;
    }
    message->issueMsg(kError, "printMonitor<file stream>: cannot"
                                " find monitor by id %d\n", monitor_id);
    return false;
}

/**
 * @brief reset monitor.
 *        reset the start_time, clear peak memory.
 * 
 * @param monitor_id 
 * @return true 
 * @return false 
 */
bool MonitorManager::resetMonitor(MonitorId monitor_id) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    auto it = monitor_map_.find(monitor_id);
    if (it != monitor_map_.end()) {
        it->second->reset();
        return true;
    }
    return false;
}

/**
 * @brief resume monitor.
 * 
 * @param monitor_id 
 * @return true 
 * @return false 
 */
bool MonitorManager::resumeMonitor(MonitorId monitor_id) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    auto it = monitor_map_.find(monitor_id);
    if (it != monitor_map_.end()) {
        it->second->resume();
        return true;
    }
    return false;
}

/**
 * @brief destory monitor.
 * 
 * @param monitor_id 
 * @return true 
 * @return false 
 */
bool MonitorManager::destroyMonitor(MonitorId monitor_id) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    auto it = monitor_map_.find(monitor_id);
    if (it != monitor_map_.end()) {
        monitor_map_.erase(it);
        return true;
    }

    return false;
}

/**
 * @brief Create a Monitor object
 * 
 * @return MonitorId 
 */
MonitorId createMonitor() {
    return kMonitorManager.createMonitor();
}

/**
 * @brief 
 * 
 * @param monitor_id 
 * @return Monitor* 
 */
Monitor* queryMonitor(MonitorId monitor_id) {
    if (kInvalidMonitorId == monitor_id) {
        return nullptr;
    }
    return kMonitorManager.queryMonitor(monitor_id);
}

/**
 * @brief 
 * 
 * @param monitor_id 
 * @param resource_types 
 * @param description 
 * @return true 
 * @return false 
 */
bool printProcessBar(MonitorId monitor_id, ResourceTypes resource_types,
          const char* description) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    return kMonitorManager.printProcessBar(monitor_id, resource_types,
                                         description);
}

/**
 * @brief 
 * 
 * @param monitor_id 
 * @param resource_types 
 * @param description 
 * @return true 
 * @return false 
 */
bool printMonitor(MonitorId monitor_id, ResourceTypes resource_types,
        const char* description) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    return kMonitorManager.printMonitor(monitor_id, resource_types,
                                         description);
}

/**
 * @brief 
 * 
 * @param monitor_id 
 * @param resource_types 
 * @param fp 
 * @param description 
 * @return true 
 * @return false 
 */
bool printMonitor(MonitorId monitor_id, ResourceTypes resource_types,
        FILE *fp, const char* description) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    return kMonitorManager.printMonitor(monitor_id, resource_types, fp,
                                         description);
}

/**
 * @brief 
 * 
 * @param monitor_id 
 * @param resource_types 
 * @param ofs 
 * @param description 
 * @return true 
 * @return false 
 */
bool printMonitor(MonitorId monitor_id, ResourceTypes resource_types,
        std::ofstream &ofs, const char* description) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    return kMonitorManager.printMonitor(monitor_id, resource_types,
                                         ofs, description);
}

/**
 * @brief 
 * 
 * @param monitor_id 
 * @return true 
 * @return false 
 */
bool pauseMonitor(MonitorId monitor_id) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    return kMonitorManager.pauseMonitor(monitor_id);
}

/**
 * @brief 
 * 
 * @param monitor_id 
 * @return true 
 * @return false 
 */
bool resumeMonitor(MonitorId monitor_id) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    return kMonitorManager.resumeMonitor(monitor_id);
}

/**
 * @brief 
 * 
 * @param monitor_id 
 * @return true 
 * @return false 
 */
bool resetMonitor(MonitorId monitor_id) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    return kMonitorManager.resetMonitor(monitor_id);
}

/**
 * @brief 
 * 
 * @param monitor_id 
 * @return true 
 * @return false 
 */
bool destroyMonitor(MonitorId monitor_id) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    return kMonitorManager.destroyMonitor(monitor_id);
}

}  // namespace util
}  // namespace open_edi
