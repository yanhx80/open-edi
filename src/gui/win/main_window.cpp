#include "main_window.h"
#include "common/action_group_manager.h"
#include "common/action_handler.h"
#include "common/action_producer.h"
#include "common/dialog_manager.h"
#include "dialog/import_dlg.h"
#include "util/util.h"
#include "widget/ribbon/ribbon.h"
#include "widget/ribbon/ribbon_file_menu.h"
#include "widget/ribbon/ribbon_title_bar.h"

#include <QApplication>
#include <QDockWidget>

namespace open_edi {
namespace gui {

MainWindow* MainWindow::instance_ = nullptr;

MainWindow::MainWindow(QWidget* parent)
  : QMainWindow(parent) {
    setObjectName("MainWindow");
    setAcceptDrops(true);

    init();
    addActions();
    createCentralWindow();
    statusBar();
}

MainWindow::~MainWindow() {
}

void MainWindow::setTclInterp(Tcl_Interp* interp) {
    interp_ = interp;
    DIALOG_MANAGER->setTclInterp(interp_);
    connect(DIALOG_MANAGER, SIGNAL(finishReadData()), this, SLOT(slotInitial()));
    // connect(DIALOG_MANAGER, SIGNAL(finishReadData()), docks->layer_widget, SLOT(refreshTree()));
}

void MainWindow::closeEvent(QCloseEvent* e) {
    hide();
    e->ignore();
}

bool MainWindow::eventFilter(QObject* obj, QEvent* e) {
    if (obj == ribbon_) {
        switch (e->type()) {
        case QEvent::MouseButtonPress:
        case QEvent::MouseButtonRelease:
        case QEvent::MouseMove:
        case QEvent::Leave:
        case QEvent::HoverMove:
        case QEvent::MouseButtonDblClick:
            QApplication::sendEvent(this, e);
            break;

        default:
            break;
        }
    }
    return QMainWindow::eventFilter(obj, e);
}

void MainWindow::init() {
    ribbon_ = new RibbonMenuBar(this);
    setMenuWidget(ribbon_);
    ribbon_->installEventFilter(this);

    action_handler_ = new ActionHandler(this);
    action_handler_->setView(graphics_view_);
    action_manager_          = new ActionGroupManager(this);
    ActionProducer* producer = new ActionProducer(this, action_handler_);
    producer->fillActionContainer(action_map_, action_manager_);
    producer->addOtherAction(action_map_, action_manager_);

    docks = new DocksManager(this);
    docks->createDockWidgets();

    QMenu* menu = new QMenu;
    foreach (auto dock, findChildren<QDockWidget*>()) {
        menu->addAction(dock->toggleViewAction());
    }
    action_map_["Widgets"]->setMenu(menu);
}

void MainWindow::addActions() {
    RibbonFileMenu* menu = qobject_cast<RibbonFileMenu*>(ribbon_->getFileButton()->menu());
    menu->addFileAction(action_map_["ImportDesign"], Qt::ToolButtonTextBesideIcon);

    ribbon_->fillActions(action_map_);
}

void MainWindow::createCentralWindow() {

    setCentralWidget(graphics_view_);
}

void MainWindow::slotInitial() {

   

    graphics_view_->refreshAllComponents();
     docks->layer_widget->refreshTree(graphics_view_->layout->getAllLayerNames());
}

} // namespace gui
} // namespace open_edi
