#ifndef IMPORT_DLG_H
#define IMPORT_DLG_H

#include <QDialog>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

namespace open_edi {
namespace gui {

class ImportDlg : public QDialog {
    Q_OBJECT

  public:
    ImportDlg(const QString& title, QWidget* parent = nullptr);
    ~ImportDlg();

  public:
    QString getLefPath();
    QString getDefPath();

  public slots:
    void slotBroswer();

  private:
    void init();

  private:
    QLineEdit* net_edit_{nullptr};
    QLineEdit* lib_edit_{nullptr};
};

} // namespace gui
} // namespace open_edi

#endif // IMPORT_DLG_H
