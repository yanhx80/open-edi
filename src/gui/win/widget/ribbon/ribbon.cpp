#include "ribbon.h"
#include "ribbon_page.h"
#include "ribbon_title_bar.h"
#include "ribbon_group.h"
#include <QLineEdit>
#include <QPalette>
#include <QColor>
#include "ribbon_file_menu.h"
#include "stacked_widget.h"
#include "ribbon_button.h"
#include "util/util.h"
#include <QDebug>

namespace open_edi {
namespace gui {

#define RIBBON_HEIGHT 150

RibbonMenuBar::RibbonMenuBar(QWidget *parent)
    : QMenuBar (parent)
    , ribbon_titlebar_(nullptr)
    , show_ribbon_(true)
{
    setObjectName("RibbonMenuBar");
    init();
    setFixedHeight(RIBBON_HEIGHT);
}

RibbonMenuBar::~RibbonMenuBar()
{

}

void RibbonMenuBar::init()
{
//    ribbon_titlebar_ = new RibbonTitleBar(this);
//    ribbon_titlebar_->setObjectName("RibbonTitleBar");

    file_button_ = new QPushButton(tr("File"), this);
    file_button_->setFocusPolicy(Qt::NoFocus);
    file_button_->setFlat(true);
    file_button_->setStyleSheet("QPushButton{background-color: blue;}");
    RibbonFileMenu* menu = new RibbonFileMenu(file_button_);
    file_button_->setMenu(menu);

    tab_bar_ = new QTabBar;
    tab_bar_->setObjectName("RibbonMenuTabBar");
//    tab_bar_->setFixedHeight(32);
    connect(tab_bar_, &QTabBar::tabBarDoubleClicked, this, &RibbonMenuBar::slotTabBarDoubleClicked);
    connect(tab_bar_, &QTabBar::currentChanged, this, &RibbonMenuBar::slotTabBarChanged);
    connect(tab_bar_, &QTabBar::tabBarClicked, this, &RibbonMenuBar::slotTabBarClicked);

    stacked_widget_ = new StackedWidget(this);
    stacked_widget_->setObjectName("RibbonMenuStackedWidget");
    stacked_widget_->installEventFilter(this);
    connect(stacked_widget_, &StackedWidget::hideWindow, this, &RibbonMenuBar::slotStackedWidgetHided);

    QHBoxLayout *tabLayout = new QHBoxLayout;
    tabLayout->addWidget(file_button_);
    tabLayout->addWidget(tab_bar_);

/*    tabLayout->addSpacerItem(new QSpacerItem(100,1, QSizePolicy::Expanding, QSizePolicy::Fixed));
    QLabel *searchLabel = new QLabel(tr("Search Instance:"));
    QLineEdit *lineEdit = new QLineEdit;
    tabLayout->addWidget(searchLabel);
    tabLayout->addWidget(lineEdit);
    */
    tabLayout->addStretch();
    RibbonButton* show_hide = new RibbonButton(this);
    tabLayout->addWidget(show_hide);
    QAction* action = new QAction(show_hide);
    action->setCheckable(true);
    action->setChecked(isRibbonHideMode());
    action->setIcon(QIcon(QString::fromStdString(open_edi::util::getInstallPath()) + "/share/etc/res/tool/down.svg"));
    action->setToolTip(tr("Hide"));
    show_hide->setDefaultAction(action);
    connect(action, &QAction::triggered, this, &RibbonMenuBar::slotPageShowHide);

    QVBoxLayout *vLayout = new QVBoxLayout;
    vLayout->setSpacing(0);
    vLayout->setMargin(0);
//    vLayout->addWidget(ribbon_titlebar_);
    vLayout->addLayout(tabLayout);
    vLayout->addWidget(stacked_widget_);

    setLayout(vLayout);
}

void RibbonMenuBar::fillActions(QMap<QString, QAction*> map){
    map_ = map;

    RibbonPage*  page  = addPage(tr("Common"));
    RibbonGroup* group = page->addGroup(tr("View"));

    group->addLargeAction(map_["Select"]);
    group->addMiniAction(map_["ZoomIn"]);
    group->addMiniAction(map_["ZoomOut"]);
    group->addMiniAction(map_["Undo"]);
    group->addMiniAction(map_["Redo"]);

    group = page->addGroup(tr("window"));
    group->addSmallAction(map_["Widgets"]);
}

RibbonPage* RibbonMenuBar::addPage(const QString &text)
{
    RibbonPage* page = new RibbonPage(this);
    page->setWindowTitle(text);

    int index = tab_bar_->addTab(text);
    tab_bar_->setTabData(index, QVariant((quint64)page));
    stacked_widget_->addWidget(page);
    connect(page, &QWidget::windowTitleChanged, this, &RibbonMenuBar::slotPageTitleChanged);

    return page;
}

int RibbonMenuBar::pageCount() const{
    return tab_bar_->count();
}

void RibbonMenuBar::setHideMode(bool hide){
    if(hide){
         stacked_widget_->setPopupMode();
         stacked_widget_->setFocusPolicy(Qt::NoFocus);
         stacked_widget_->clearFocus();
         tab_bar_->setFocus();
         stacked_widget_->hide();
         setFixedHeight(tab_bar_->geometry().bottom());
    }else {
         stacked_widget_->setNormalMode();
         stacked_widget_->setFocus();
         stacked_widget_->show();
         setFixedHeight(RIBBON_HEIGHT);
    }
    update();
}

bool RibbonMenuBar::isRibbonHideMode() const{
    return stacked_widget_->isPopupMode();
}

void RibbonMenuBar::slotStackedWidgetHided(){
    tab_bar_->setCurrentIndex(-1);
}

void RibbonMenuBar::slotPageTitleChanged(const QString &text){
   QWidget *w = qobject_cast<QWidget*>(sender());

   for(int i = 0; i < tab_bar_->count(); i++){
       QVariant var = tab_bar_->tabData(i);
       QWidget *page = nullptr;
       if(var.isValid()){
           page = (QWidget*)(var.value<quint64>());
           if(page == w){
               tab_bar_->setTabText(i, text);
           }
       }
   }
}

void RibbonMenuBar::slotTabBarDoubleClicked(int index){
    Q_UNUSED(index)
    setHideMode(!isRibbonHideMode());
}

void RibbonMenuBar::slotTabBarClicked(int index){
    if(isRibbonHideMode()){
        if(!stacked_widget_->isVisible()){
            if(stacked_widget_->isPopupMode()){
                tab_bar_->setCurrentIndex(index);
            }
        }
    }
}

void RibbonMenuBar::slotTabBarChanged(int index){
    QVariant var = tab_bar_->tabData(index);
    RibbonPage *page = nullptr;

    if(var.isValid()){
        page = (RibbonPage*)(var.value<quint64>());
    }
    if(page){
        if(stacked_widget_->currentWidget() != page){
            stacked_widget_->setCurrentWidget(page);
        }
        if(isRibbonHideMode()){
            if(!stacked_widget_->isVisible()){
                if(stacked_widget_->isPopupMode()){
                    QPoint pos(mapToGlobal(QPoint(tab_bar_->rect().left(), tab_bar_->rect().bottom()+1)));
                    stacked_widget_->exec();
                    stacked_widget_->setGeometry(pos.x(), pos.y(), width(), stacked_widget_->height());
                }
            }
        }
    }
    emit currentTabChanged(index);
}

void RibbonMenuBar::slotPageShowHide(bool show){
    setHideMode(show);
    QAction* action = qobject_cast<QAction*>(sender());
    if(isRibbonHideMode()){
        action->setIcon(QIcon(QString::fromStdString(open_edi::util::getInstallPath()) + "/share/etc/res/tool/down.svg"));
        action->setToolTip(tr("Show"));
    }else {
        action->setIcon(QIcon(QString::fromStdString(open_edi::util::getInstallPath()) + "/share/etc/res/tool/up.png"));
        action->setToolTip(tr("Hide"));
    }
}

RibbonTitleBar* RibbonMenuBar::getTitleBar() const{
    return ribbon_titlebar_;
}

QPushButton* RibbonMenuBar::getFileButton() const{
    return file_button_;
}

}
}
