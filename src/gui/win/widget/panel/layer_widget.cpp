
#include "layer_widget.h"

namespace open_edi {
namespace gui {

LayerWidget::LayerWidget(QWidget* parent) : TreeWidgetBase(parent) {

    pal = new Palette(
      {
        QBrush("White"),
        QBrush("Red"),
        QBrush("Green"),
        QBrush("Blue"),
        QBrush("Yellow"),
        QBrush("#FF00FF"),
        QBrush("#00FFFF"),
        QBrush("#780078"),
        QBrush("#FFC0C0"),
        QBrush("#FFA800"),
      },
      true);

    pal->setName("Color: ");

    layer_top_item = __createTopItem(this, "layer");

    connect(this, &LayerWidget::itemClicked, this, &LayerWidget::slotItemClicked);
    connect(pal, &Palette::signalBtnOKClicked, this, &LayerWidget::slotItemColorChange);
}

LayerWidget::~LayerWidget() {
}

void LayerWidget::addLayerListener(LayerListener* listener) {
    layer_listener_list_.append(listener);
}

void LayerWidget::slotItemColorChange(const char* item_name, QBrush brush) {
    QString name(item_name);
    if (lable_map_.find(name) != lable_map_.end()) {
        lable_map_[name]->setBrush(brush);
        lable_map_[name]->update();
    }
}

void LayerWidget::refreshTree(QList<QString> name_list) {

    QBrush brush_array[10]{
      QBrush("White"),
      QBrush("Red"),
      QBrush("Green"),
      QBrush("Blue"),
      QBrush("Yellow"),
      QBrush("#FF00FF"),
      QBrush("#00FFFF"),
      QBrush("#780078"),
      QBrush("#FFC0C0"),
      QBrush("#FFA800")};
    int i = 0;
    for (auto& name : name_list) {
        __createSubItems(layer_top_item,
                         {
                           {ItemAttributes{name.toLocal8Bit().constData(),
                                           brush_array[i++ % 10]}},
                         });
    }
}

} // namespace gui
} // namespace open_edi