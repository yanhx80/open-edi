#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QAction>
#include <QGraphicsView>
#include <QMainWindow>
#include <QMap>
#include <QMdiArea>
#include "./common/docks_manager.h"
#include "./layout/graphics_view.h"
#include "tcl.h"

namespace open_edi {
namespace gui {

#define MAIN_WINDOW MainWindow::getInstance()

class ActionHandler;
class MDIWindow;
class RibbonMenuBar;
class ActionGroupManager;

class MainWindow : public QMainWindow {
    Q_OBJECT

  public:
    static MainWindow* getInstance() {
        if (!instance_) {
            instance_ = new MainWindow;
        }
        return instance_;
    }
    void setTclInterp(Tcl_Interp* intrep);

    void closeEvent(QCloseEvent* e) override;

  protected:
    bool eventFilter(QObject* obj, QEvent* e);

  private:
    void init();
    void addActions();
    void createCentralWindow();
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

  signals:
    void windowChanged(bool);

  private:
    static MainWindow*      instance_;
    QMdiArea*               mdi_area_{nullptr};
    MDIWindow*              current_subwindow_{nullptr};
    QList<MDIWindow*>       window_list_{nullptr};
    RibbonMenuBar*          ribbon_{nullptr};
    ActionHandler*          action_handler_{nullptr};
    QMap<QString, QAction*> action_map_;
    ActionGroupManager*     action_manager_{nullptr};
    GraphicsView*           graphics_view_{LAYOUT_GRAPHICS_VIEW};
    Tcl_Interp*             interp_{nullptr};
    DocksManager*           docks{nullptr};

  private slots:
    void slotInitial();
};

} // namespace gui
} // namespace open_edi
#endif // MAINWINDOW_H
