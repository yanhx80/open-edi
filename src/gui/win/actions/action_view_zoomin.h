#ifndef ACTION_VIEW_ZOOMIN_H
#define ACTION_VIEW_ZOOMIN_H

#include "action_abstract.h"

namespace open_edi {
namespace gui {

class ActionViewZoomin : public ActionAbstract {
    Q_OBJECT
  public:
    ActionViewZoomin(GraphicsView &view, QObject* parent = nullptr);
    virtual ~ActionViewZoomin();
};

} // namespace gui
} // namespace open_edi

#endif // ACTION_VIEW_ZOOMIN_H
