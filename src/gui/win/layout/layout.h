#include <QColor>
#include <QList>
#include <QString>
#include <QTime>
#include <qmath.h>

#include "../widget/panel/components_listener.h"
#include "../widget/panel/layer_listener.h"
#include "db/core/cell.h"
#include "db/core/db.h"
#include "db/io/write_def.h"
#include "db/util/array.h"
#include "db/util/property_definition.h"
#include "graphics_scene.h"
#include "items/li_die_area.h"
#include "items/li_instance.h"
#include "items/li_manager.h"
#include "items/li_net.h"
#include "util/util.h"

namespace open_edi {
namespace gui {

#define LAYOUT_INSTANCE (Layout::getInstance())

class Layout : public LayerListener,
               public ComponentListener {
  public:
    ~Layout();

    static Layout* getInstance() {
        if (!inst_) {
            inst_ = new Layout;
        }
        return inst_;
    }

    LI_DieArea*   li_die_area;
    LI_Instance*  li_instances;
    LI_Pin*       li_pins;
    LI_Manager*   li_manager{LI_MANAGER};

    void            setViewSize(int w, int h);
    QList<QString>& getAllLayerNames();
    void            refreshAllComponents();
    int             getDieAreaW() { return die_area_w_; };
    int             getDieAreaH() { return die_area_h_; };
    int             getScaleFactor() { return scale_factor_; };
    void            createLayoutItems();
    int             caculateDieArea();

    virtual void setLayerVisible(QString name, bool v) override;
    virtual void setLayerColor(QString name, QColor color) override;
    virtual void setLayerSelectable(QString name, bool v) override;
    virtual void setComponentVisible(QString name, bool v) override;
    virtual void setComponentSelectable(QString name, bool v) override;
    virtual void setComponentBrushStyle(QString name, Qt::BrushStyle brush_style) override;

  private:
    int            die_area_w_{1};
    int            die_area_h_{1};
    int            view_width_{1};
    int            view_height_{1};
    int            scale_factor_{1};
    static Layout* inst_;
};
} // namespace gui
} // namespace open_edi