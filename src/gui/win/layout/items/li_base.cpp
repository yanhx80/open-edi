#include "li_base.h"

namespace open_edi {
namespace gui {

void LI_Base::refreshBoundSize() {
    if (bound_width_ != getGraphicItem()->scene()->width()
        || bound_height_ != getGraphicItem()->scene()->height()) {
        bound_width_  = getGraphicItem()->scene()->width();
        bound_height_ = getGraphicItem()->scene()->height();

        QTransform matrix;
        matrix.translate(-bound_width_ / 2, -bound_height_ / 2);
        getGraphicItem()->setTransform(matrix);
#if DRAW_MODE == 1
        if (img_) {
            delete img_;
            img_ = new QPixmap(bound_width_, bound_height_);
        } else {
            img_ = new QPixmap(bound_width_, bound_height_);
        }
#endif
    }
}

void LI_Base::setVisible(bool visible) {
    visible_ = visible;
    getGraphicItem()->setVisible(visible_);
}

void LI_Base::draw(QPainter* painter) {
#if DRAW_MODE == 1
    if (img_ && isVisible()) {
        painter->drawPixmap(0, 0, *img_);
    }
#elif DRAW_MODE == 2
    painter->drawPath(painter_path_);
#endif
    auto draw_mode = DRAW_MODE;
    printf("draw mode %s\n",
           draw_mode == IMG_MODE ? "image mode" :
                                   draw_mode == PATH_MODE ? "path mode" :
                                                            "no draw mode");
}

void LI_Base::update() {
    getGraphicItem()->update();
}

QString LI_Base::getName() {
    return name_;
}
} // namespace gui
} // namespace open_edi