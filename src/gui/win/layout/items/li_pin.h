#ifndef EDI_GUI_LI_PINS_H_
#define EDI_GUI_LI_PINS_H_

#include <QPainter>
#include <qmath.h>
#include "../graphicitems/lgi_pin.h"
#include "../graphics_scene.h"
#include "db/core/cell.h"
#include "db/core/db.h"
#include "db/io/write_def.h"
#include "db/util/array.h"
#include "db/util/property_definition.h"
#include "li_base.h"
#include "util/util.h"

namespace open_edi {
namespace gui {
class LI_Pin : public LI_Base {
  public:
    explicit LI_Pin(int* scale_factor);
    LI_Pin(const LI_Pin& other) = delete;
    LI_Pin& operator=(const LI_Pin& rhs) = delete;
    ~LI_Pin();

    virtual void preDraw() override;
    LGI_Pin*    getGraphicItem();
    void         drawPins(open_edi::db::Inst& ins);
    void         fillImage();
    QPixmap*     getImg() { return img_; };

  protected:
    virtual void draw(QPainter* painter);

  private:
    LGI_Pin* item_;
    QPainter  painter_;
};
} // namespace gui
} // namespace open_edi

#endif
