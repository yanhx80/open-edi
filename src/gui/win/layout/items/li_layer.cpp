#include "li_layer.h"
namespace open_edi {
namespace gui {
LI_Layer::LI_Layer() : LI_Base() {
    item_ = new LGI_Layer;
    item_->setLiBase(this);
    type = kLayer;
    visible_ = false;
}

LI_Layer::LI_Layer(QString name) : LI_Base(name) {

    LI_Layer();
}

LI_Layer::~LI_Layer() 
{
    
}

void LI_Layer::preDraw() {
}

bool LI_Layer::hasSubLI() {
    return false;
}

LGI_Layer* LI_Layer::getGraphicItem() {
    return item_;
}

bool LI_Layer::isMainLI() {
    return false;
}

void LI_Layer::draw(QPainter* painter) 
{
    
}
} // namespace gui
} // namespace open_edi