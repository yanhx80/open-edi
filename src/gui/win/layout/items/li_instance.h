#ifndef EDI_GUI_LI_INSTANCES_H_
#define EDI_GUI_LI_INSTANCES_H_

#include <QPainter>
#include <qmath.h>
#include "../graphicitems/lgi_instance.h"
#include "../graphics_scene.h"
#include "db/core/cell.h"
#include "db/core/db.h"
#include "db/io/write_def.h"
#include "db/util/array.h"
#include "db/util/property_definition.h"
#include "li_base.h"
#include "li_pin.h"
#include "util/util.h"

namespace open_edi {
namespace gui {
class LI_Instance : public LI_Base {
  public:
    explicit LI_Instance(int* scale_factor);
    LI_Instance(const LI_Instance& other) = delete;
    LI_Instance& operator=(const LI_Instance& rhs) = delete;
    ~LI_Instance();

    LI_Pin* li_pins;

    virtual void   preDraw() override;
    virtual bool   hasSubLI() override;
    LGI_Instance* getGraphicItem() override;
    virtual bool   isMainLI() override;

  protected:
    virtual void draw(QPainter* painter);

  private:
    LGI_Instance* item_;
};
} // namespace gui
} // namespace open_edi

#endif
