#ifndef EDI_GUI_LI_LAYER_H_
#define EDI_GUI_LI_LAYER_H_

#include <QPainter>
#include <qmath.h>
#include "../graphicitems/lgi_layer.h"
#include "../graphics_scene.h"
#include "db/core/cell.h"
#include "db/core/db.h"
#include "db/io/write_def.h"
#include "db/util/array.h"
#include "db/util/property_definition.h"
#include "util/util.h"
#include "li_base.h"


namespace open_edi {
namespace gui {
class LI_Layer : public LI_Base {
  public:
    LI_Layer();
    explicit LI_Layer(QString name);
    ~LI_Layer();

    virtual void   preDraw() override;
    virtual bool   hasSubLI() override;
    LGI_Layer* getGraphicItem() override;
    virtual bool   isMainLI() override;

  protected:
    virtual void draw(QPainter* painter);

  private:
    LGI_Layer* item_;
};
} // namespace gui
} // namespace open_edi

#endif