#ifndef EDI_GUI_LI_MANAGER_H_
#define EDI_GUI_LI_MANAGER_H_

#include <QList>
#include <QMap>
#include <QString>

#include "db/core/cell.h"
#include "db/core/db.h"
#include "db/io/write_def.h"
#include "db/util/array.h"
#include "db/util/property_definition.h"


namespace open_edi {
namespace gui {

#define LI_MANAGER (LI_Manager::getInstance())

class LI_Base;

class LI_Manager {
  public:
    ~LI_Manager() = delete;
    static LI_Manager* getInstance() {
        if (!inst_) {
            inst_ = new LI_Manager;
        }
        return inst_;
    }

    void            preDrawAllItems();
    QList<LI_Base*> getLiList();
    void            addLI(LI_Base* li);
    void            setLiVisibleByName(QString name, bool v);
    void            setLiBrushStyleByName(QString name, Qt::BrushStyle brush_style);
    LI_Base*        getLiByName(QString name);
    void            addLayer(open_edi::db::Layer* layer);
    void            setLayerVisibleByName(QString name, bool v);
    void            setLayerColorByName(QString name, QColor color);
    LI_Base*        getLayerByName(QString name);
    QList<QString>& getLayerNameList();

  private:
    LI_Manager();
    static LI_Manager*          inst_;
    QList<open_edi::db::Layer*> layer_list_;
    QList<QString>              layer_name_list_;
    QMap<QString, LI_Base*>     layer_map_;
    QList<LI_Base*>             li_list_;
    QMap<QString, LI_Base*>     li_map_;
    int*                        scale_factor_;
};
} // namespace gui
} // namespace open_edi

#endif