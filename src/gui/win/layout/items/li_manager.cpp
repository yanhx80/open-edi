#include "li_manager.h"
#include "li_base.h"
#include "li_layer.h"
namespace open_edi {
namespace gui {

LI_Manager::LI_Manager() {
}

void LI_Manager::preDrawAllItems() {
    for (auto item : li_list_) {
        if (item->isMainLI()) {
            item->preDraw();
        }
    }
}

QList<LI_Base*> LI_Manager::getLiList() {
    return li_list_;
}

void LI_Manager::addLI(LI_Base* li) {
    li_list_.append(li);
    li_map_[li->getName()] = li;
}

void LI_Manager::setLiVisibleByName(QString name, bool v) {

    if (li_map_.find(name) != li_map_.end()) {
        auto li = li_map_[name];
        li->setVisible(v);
    }
}

void LI_Manager::setLiBrushStyleByName(QString name, Qt::BrushStyle brush_style) {
    if (li_map_.find(name) != li_map_.end()) {
        auto li = li_map_[name];
        li->setBrushStyle(brush_style);
    }
}

LI_Base* LI_Manager::getLiByName(QString name) {
    if (li_map_.find(name) != li_map_.end()) {
        return li_map_[name];
    }
    return nullptr;
}

void LI_Manager::addLayer(open_edi::db::Layer* layer) {
    layer_list_.append(layer);
    auto name = layer->getName();
    printf("%s\n", name);
    layer_name_list_.append(name);
    // layer_map_[name] = new LI_Layer(name);
}

void LI_Manager::setLayerVisibleByName(QString name, bool v) {
}

void LI_Manager::setLayerColorByName(QString name, QColor color) {
}

LI_Base* LI_Manager::getLayerByName(QString name) {
    return nullptr;
}

QList<QString>& LI_Manager::getLayerNameList() {
    return layer_name_list_;
}

LI_Manager* LI_Manager::inst_ = nullptr;
} // namespace gui
} // namespace open_edi