#include "li_instance.h"

namespace open_edi {
namespace gui {

LI_Instance::LI_Instance(int* scale_factor) : LI_Base(scale_factor) {
    item_ = new LGI_Instance;
    item_->setLiBase(this);
    pen_.setColor(QColor("#909090"));
    type    = kInstance;
    li_pins = new LI_Pin(scale_factor);
    name_   = "Instance";
    li_mgr_->addLI(this);

    //defualt visible
    visible_ = true;
}

LI_Instance::~LI_Instance() {
}

LGI_Instance* LI_Instance::getGraphicItem() {
    return item_;
}

bool LI_Instance::isMainLI() {
    return true;
}

void LI_Instance::draw(QPainter* painter) {
    painter->setPen(pen_);
    LI_Base::draw(painter);
}

void LI_Instance::preDraw() {
    if (!visible_) {
        return;
    }
    refreshBoundSize();
    li_pins->refreshBoundSize();
    li_pins->fillImage();
#if DRAW_MODE == 1
    img_->fill(Qt::transparent);
    QPainter painter(img_);
    painter.setPen(pen_);
    painter.setWindow(0,
                      bound_height_,
                      bound_width_ + VIEW_SPACE,
                      -bound_height_ - VIEW_SPACE);
#elif DRAW_MODE == 2
    painter_path_ = QPainterPath();
#endif


    auto     tc               = open_edi::db::getTopCell();
    uint64_t num_components   = tc->getNumOfInsts();
    auto     components       = tc->getInstances();
    auto     component_vector = open_edi::db::Object::addr<open_edi::db::ArrayObject<open_edi::db::ObjectId>>(components);

    auto factor = *scale_factor_;
    // printf("COMPONENTS %d ;\n", num_components);
    for (auto iter = component_vector->begin(); iter != component_vector->end(); ++iter) {
        auto instance = open_edi::db::Object::addr<open_edi::db::Inst>(*iter);
        auto insbox   = instance->getBox();
        auto insllx   = insbox.getLLX();
        auto inslly   = insbox.getLLY();
        auto insurx   = insbox.getURX();
        auto insury   = insbox.getURY();
        auto width    = insurx - insllx;
        auto height   = insury - inslly;
#if DRAW_MODE == 1
        if (width >= (factor >> 2) || height >= (factor >> 2)) {
            painter.drawRect(QRectF(
              (insllx) / factor,
              (inslly) / factor,
              width / factor,
              height / factor));
            if (width > (factor << 2) && height > (factor << 2)) {
                li_pins->drawPins(*instance);
            }
        }
#elif DRAW_MODE == 2
        if (width >= factor >> 2 || height >= factor >> 2) {
            painter_path_.addRect(QRectF(
              (insllx) / factor,
              (inslly) / factor,
              width / factor,
              height / factor));
            if (width > factor * 4 && height > factor * 4) {
                li_pins->drawPins(*instance);
            }
        }
#endif
    }

    item_->setMap(img_);
    item_->setItemSize(bound_width_, bound_height_);

    li_pins->getGraphicItem()->setMap(img_);
    li_pins->getGraphicItem()->setItemSize(bound_width_, bound_height_);
}

bool LI_Instance::hasSubLI() {
    return true;
}

} // namespace gui
} // namespace open_edi