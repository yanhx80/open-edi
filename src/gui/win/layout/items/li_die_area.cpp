#include <QPainter>

#include "li_die_area.h"

namespace open_edi {
namespace gui {

LI_DieArea::LI_DieArea(int* scale_factor) : LI_Base(scale_factor_) {
    item_ = new LGI_DieArea;
    item_->setLiBase(this);
    pen_.setColor(QColor("#909090"));
    name_ = "Die area";
    type = kDieArea;
    li_mgr_->addLI(this);
    setVisible(true);
    item_->setVisible(true);
}

LI_DieArea::~LI_DieArea() {
}

void LI_DieArea::preDraw() {
    refreshBoundSize();
#if DRAW_MODE== 1
    img_->fill(Qt::transparent);
    QPainter painter(img_);
    painter.setPen(pen_);
    painter.setWindow(0,
                      bound_height_,
                      bound_width_ + VIEW_SPACE,
                      -bound_height_ - VIEW_SPACE);
    painter.drawRect(QRectF(0, 0, bound_width_, bound_height_));
#elif DRAW_MODE== 2
    painter_path_ = QPainterPath();
    painter_path_.addRect(QRectF(0, 0, bound_width_, bound_height_));
#endif
    item_->setMap(img_);
    item_->setItemSize(bound_width_, bound_height_);
}

LGI_DieArea* LI_DieArea::getGraphicItem() {
    return item_;
}

bool LI_DieArea::isMainLI() 
{
    return true;
}


void LI_DieArea::draw(QPainter* painter) 
{  
    painter->setPen(pen_);
    LI_Base::draw(painter);
}

} // namespace gui
} // namespace open_edi