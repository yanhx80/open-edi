#ifndef LAYOUT_GRAPHICS_VIEW_H
#define LAYOUT_GRAPHICS_VIEW_H

#define USE_OPENGL 0

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPainterPath>
#include <QWheelEvent>
#if USE_OPENGL == 1
#include <QtOpenGL>
#endif
#include <qmath.h>
#include "../widget/panel/components_listener.h"
#include "../widget/panel/layer_listener.h"
#include "db/core/cell.h"
#include "db/core/db.h"
#include "db/io/write_def.h"
#include "db/util/array.h"
#include "db/util/property_definition.h"
#include "graphics_scene.h"
#include "layout.h"

namespace open_edi {
namespace gui {

#define LAYOUT_GRAPHICS_VIEW (GraphicsView::getInstance())

class GraphicsView : public QGraphicsView {
    Q_OBJECT
  public:
    ~GraphicsView();

    static GraphicsView* getInstance() {
        if (!inst_) {
            inst_ = new GraphicsView;
        }
        return inst_;
    }

    void refreshAllComponents();

    Layout* layout;

  signals:
    void sendPos(int x, int y);

  public slots:
    void slotZoomIn(bool);
    void slotZoomOut(bool);

    void slotReadLayer();
    void setPinsVisible(bool);
    void slotInitial();

  protected:
    virtual void wheelEvent(QWheelEvent* event) override;
    virtual void mouseMoveEvent(QMouseEvent* event) override;

  private:
    void                 zoomBy_(qreal factor);
    int                  view_width_;
    int                  view_height_;
    GraphicsScene*       scene_;
    static GraphicsView* inst_;

    GraphicsView(QWidget* parent = nullptr);

    void __zoom(qreal value);
};
} // namespace gui
} // namespace open_edi
#endif
