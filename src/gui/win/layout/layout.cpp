#include "layout.h"
namespace open_edi {
namespace gui {
Layout::~Layout() {
}

void Layout::setViewSize(int w, int h) {
    view_width_  = w;
    view_height_ = h;

    caculateDieArea();
}

QList<QString>& Layout::getAllLayerNames() {
    auto tc         = open_edi::db::getTopCell();
    auto tech_lib   = tc->getTechLib();
    auto num_layers = tech_lib->getNumLayers();
    printf("number of laryers: %d\n", num_layers);
    for (int i = 0; i < num_layers; i++) {
        auto layer = tech_lib->getLayer(i);
        li_manager->addLayer(layer);
    }

    return li_manager->getLayerNameList();
}

void Layout::refreshAllComponents() {
    QTime time;
    time.start();

    li_manager->preDrawAllItems();

    auto time_elapsed = time.elapsed();
    printf("elapsed time %d (ms)\n", time_elapsed);
}

void Layout::createLayoutItems() {
    LI_Base* li = new LI_DieArea(&scale_factor_);
    li          = new LI_Instance(&scale_factor_);
    li          = new LI_Net(&scale_factor_);
}

int Layout::caculateDieArea() {
    auto tc = open_edi::db::getTopCell();

    auto  poly = tc->getFloorplan()->getDieAreaPolygon();
    float factor_x, factor_y;

    auto numPoints = poly->getNumPoints();

    //find die area
    for (int i = 0; i < numPoints; i++) {
        auto point   = poly->getPoint(i);
        auto point_x = point.getX();
        auto point_y = point.getY();

        // printf("pointx %d, pointy %d\n", point_x, point_y);
        if (point_x && point_y) {
            factor_x      = (point_x) / view_width_;
            factor_y      = (point_y) / view_height_;
            scale_factor_ = factor_x > factor_y ?
                              qCeil(factor_x) :
                              qCeil(factor_y);
            die_area_w_ = qCeil((point_x) / scale_factor_);
            die_area_h_ = qCeil((point_y) / scale_factor_);
        }
    }

    return scale_factor_;
}

void Layout::setLayerVisible(QString name, bool v) {
}

void Layout::setLayerColor(QString name, QColor color) {
}

void Layout::setLayerSelectable(QString name, bool v) {
}

void Layout::setComponentVisible(QString name, bool v) {
    li_manager->setLiVisibleByName(name, v);
    // li_manager->preDrawAllItems();
    li_manager->getLiByName(name)->preDraw();
    li_manager->getLiByName(name)->update();
}

void Layout::setComponentSelectable(QString name, bool v) {
}

void Layout::setComponentBrushStyle(QString name, Qt::BrushStyle brush_style) {
    li_manager->setLiBrushStyleByName(name, brush_style);
    li_manager->getLiByName(name)->preDraw();
    li_manager->getLiByName(name)->update();
}

Layout* Layout::inst_ = nullptr;
} // namespace gui
} // namespace open_edi