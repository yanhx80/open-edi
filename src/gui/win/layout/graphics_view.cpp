#include "graphics_view.h"

namespace open_edi {
namespace gui {

GraphicsView::GraphicsView(QWidget* parent) : QGraphicsView(parent) {

    setMinimumSize(700 + VIEW_SPACE, 500 + VIEW_SPACE);

    setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    setOptimizationFlags(QGraphicsView::DontSavePainterState);
    setMouseTracking(true);
    layout = LAYOUT_INSTANCE;

    layout->createLayoutItems();

    scene_ = new GraphicsScene;
    for (auto li : layout->li_manager->getLiList()) {
        scene_->addItem(li->getGraphicItem());
    }

    setScene(scene_);

    view_width_  = width() - VIEW_SPACE;
    view_height_ = height() - VIEW_SPACE;

#if USE_OPENGL == 1
    setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers)));
#endif

    // setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    // setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

GraphicsView::~GraphicsView() {
}

void GraphicsView::slotInitial() {
    refreshAllComponents();
    slotReadLayer();
}

void GraphicsView::__zoom(qreal value) {

    qreal s = value;
    view_width_ *= s;
    view_height_ *= s;

    refreshAllComponents();
}

void GraphicsView::slotZoomIn(bool) {
    __zoom(0.8);
}

void GraphicsView::slotZoomOut(bool) {
    __zoom(1.2);
}

void GraphicsView::refreshAllComponents() {

    layout->setViewSize(view_width_, view_height_);

    scene_->setSceneRect((-layout->getDieAreaW()) / 2,
                         (-layout->getDieAreaH()) / 2,
                         layout->getDieAreaW(),
                         layout->getDieAreaH());

    layout->refreshAllComponents();

    viewport()->update();
}

void GraphicsView::slotReadLayer() {
    // layout->getAllLayerNames();
}

void GraphicsView::setPinsVisible(bool visible) {
    layout->li_pins->setVisible(visible);
}

void GraphicsView::wheelEvent(QWheelEvent* event) {
    qreal fatory = qPow(1.1, event->delta() / 240.0);
    zoomBy_(fatory);
}

void GraphicsView::mouseMoveEvent(QMouseEvent* event) {
    auto point = mapToScene(event->pos());
    auto x     = (point.rx() + scene_->width() / 2) * layout->getScaleFactor();
    auto y     = -(point.ry() - scene_->height() / 2) * layout->getScaleFactor();
    emit sendPos(x, y);
}

void GraphicsView::zoomBy_(qreal factor) {
    // scale(factor, factor);
    __zoom(factor);
}

GraphicsView* GraphicsView::inst_ = nullptr;
} // namespace gui
} // namespace open_edi